from .array import array
from .linear_space import linear_space
from .matrix_cross import matrix_cross
from .matrix_determinant import matrix_determinant
from .matrix_diagonal import matrix_diagonal
from .matrix_divide import matrix_divide
from .matrix_dot import matrix_dot
from .matrix_eigenvalue import matrix_eigenvalue
from .matrix_inverse import matrix_inverse
from .matrix_multiply import matrix_multiply
from .matrix_transpose import matrix_transpose
from .ones import ones
from .reshape import reshape
from .shape import shape
from .zeros import zeros

matrix = array
