import numpy

from .exceptions import LinearAlgebraError


def matrix_eigenvalue(a: numpy.ndarray):
    """
    计算一个矩阵的特征值和特征向量
    :param a:
    :return: (特征值一维矩阵, 每一列是一个特征向量的二维矩阵）
    """
    assert len(a.shape) == 2, f'Eigenvalue is only valid for 2D matrix, not "{a.shape}"'
    try:
        return numpy.linalg.eig(a)
    except numpy.linalg.LinAlgError as error:
        raise LinearAlgebraError(error.args[0])
