import numpy


def matrix_multiply(a: numpy.ndarray, b: numpy.ndarray) -> numpy.ndarray:
    """
    矩阵的乘法运算
    :param a:
    :param b:
    :return:
    """
    assert isinstance(a, numpy.ndarray), f'param a should be `numpy.ndarray` instance, not "{type(a)}"'
    assert isinstance(b, numpy.ndarray), f'param b should be `numpy.ndarray` instance, not "{type(b)}"'
    assert len(a.shape) == 2, f'array a is not matrix, with shape "{a.shape}"'
    assert len(b.shape) == 2, f'array b is not matrix, with shape "{b.shape}"'
    return numpy.matmul(a, b)
