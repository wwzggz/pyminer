# 线性代数算法包

线性代数算法包主要用于解决以下问题：

1. 生成常见矩阵；
    1. zeros: 零矩阵；
    1. ones: 1矩阵；
    1. magic: 幻方矩阵（未找到对应实现，在`miner2/core/__init__`下有一个临时的实现需要迁移）；
    1. range: 按自然数生成的矩阵（未实现）；
    1. array/matrix: 根据列表生成矩阵；
    1. linear_space: 生成等间距的向量；
    1. log_space: 生成对数间距的向量（未实现）；
    1. matrix_diagonal: 生成对角矩阵或者获取对角元素；
1. 实现矩阵的基本运算：
    1. sum: 矩阵的和（未实现）；
    1. product: 矩阵的积（未实现）；
    1. reshape: 改变矩阵的形状；
    1. matrix_transpose: 转置；
    1. matrix_dot: 点积；
    1. matrix_cross: 叉积；
    1. matrix_multiply: 矩阵的乘法；
    1. matrix_divide: 矩阵的除法；
    1. matrix_inverse: 矩阵的逆；
1. 进行矩阵的分解：
    1. matrix_decomposition: 矩阵分解（未实现）；
    1. matrix_triangular_upper: 获取矩阵的上三角部分（未实现）；
    1. matrix_triangular_lower: 获取矩阵的下三角部分（未实现）；
    1. matrix_diagonal: 获取矩阵的对角线部分/按对角线生成矩阵（未实现）；
1. 判断矩阵的结构：
    1. is_matrix_diagonal: 判断矩阵是否是对角矩阵（未实现）；
    1. is_matrix_symmetric: 判断矩阵是否是对称矩阵（未实现）；
    1. is_matrix_triangular_upper: 判断矩阵是否是上三角矩阵（未实现）；
    1. is_matrix_triangular_lower: 判断矩阵是否是下三角矩阵（未实现）；
1. 计算矩阵的属性：
    1. shape: 获取矩阵的结构；
    1. matrix_determinant: 计算矩阵的行列式；
    1. matrix_eigenvalue: 计算矩阵的特征值及特征向量；
    1. matrix_condition_number: 计算矩阵的条件数（未实现）；

## 备注

关于函数命名的考虑：
由于函数库采用统一的命名空间，为了避免与其他函数发生命名冲突，
暂是考虑对于仅对二维矩阵进行操作的函数通过添加一个`matrix`前缀进行区分。
在`PyMiner`的算法库中，`numpy.ndarray`应当是第一类对象，
因此对任意维度的矩阵进行操作时，不加前缀。
**由于能力限制，不清楚高维空间内是否也存在这些矩阵相关的函数**，
如果哪里写的不对还望各位大佬指教。

如果您有很强的线性代数基础，欢迎您和我一起进行此算法包的封装。
线性代数博大精深，在下以一个工科生的线性代数水平，显然是不足以完成此包的。
关于范数等内容，在下没有能力实现，还望您的帮助。

# 参考文献

1. [MATLAB中的线性代数工具箱. MATLAB.][matlab]
1. [Numpy中的线性代数工具箱. Numpy.][numpy]

[matlab]: https://ww2.mathworks.cn/help/matlab/linear-algebra.html
[numpy]: https://numpy.org/doc/stable/reference/routines.linalg.html

